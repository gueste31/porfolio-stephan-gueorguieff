import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { ProjetComponent } from './projet/projet.component';
import { VeilleComponent } from './veille/veille.component';
import { HomeComponent } from './home/home.component';
import { ParcoursComponent } from './parcours/parcours.component';
import { ContactComponent } from './contact/contact.component';
import { ProjetListComponent } from './projet/projet-list/projet-list.component';
import { VeilleListComponent } from './veille/veille-list/veille-list.component';
import { SatyvaComponent } from './projet/projet-list/satyva/satyva.component';
import { AdministrationComponent } from './projet/projet-list/administration/administration.component';
import { PortfolioComponent } from './projet/projet-list/portfolio/portfolio.component';
import { BilanSocialComponent } from './projet/projet-list/bilan-social/bilan-social.component';
import { AmpComponent } from './projet/projet-list/amp/amp.component';
import { EComboxComponent } from './projet/projet-list/e-combox/e-combox.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    ProjetComponent,
    VeilleComponent,
    HomeComponent,
    ParcoursComponent,
    ContactComponent,
    ProjetListComponent,
    VeilleListComponent,
    SatyvaComponent,
    AdministrationComponent,
    PortfolioComponent,
    BilanSocialComponent,
    AmpComponent,
    EComboxComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
