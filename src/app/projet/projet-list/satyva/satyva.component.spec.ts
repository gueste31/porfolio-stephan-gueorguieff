import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SatyvaComponent } from './satyva.component';

describe('SatyvaComponent', () => {
  let component: SatyvaComponent;
  let fixture: ComponentFixture<SatyvaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SatyvaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SatyvaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
