import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BilanSocialComponent } from './bilan-social.component';

describe('BilanSocialComponent', () => {
  let component: BilanSocialComponent;
  let fixture: ComponentFixture<BilanSocialComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BilanSocialComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BilanSocialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
