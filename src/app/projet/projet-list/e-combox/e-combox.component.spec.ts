import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EComboxComponent } from './e-combox.component';

describe('EComboxComponent', () => {
  let component: EComboxComponent;
  let fixture: ComponentFixture<EComboxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EComboxComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EComboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
