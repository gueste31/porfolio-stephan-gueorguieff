import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {ProjetListComponent} from "./projet/projet-list/projet-list.component";
import {VeilleListComponent} from "./veille/veille-list/veille-list.component";
import {SatyvaComponent} from "./projet/projet-list/satyva/satyva.component";
import {PortfolioComponent} from "./projet/projet-list/portfolio/portfolio.component";
import {AdministrationComponent} from "./projet/projet-list/administration/administration.component";
import {BilanSocialComponent} from "./projet/projet-list/bilan-social/bilan-social.component";
import {AmpComponent} from "./projet/projet-list/amp/amp.component";

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'projet', component: ProjetListComponent },
  { path: 'veille-technologique', component: VeilleListComponent },
  { path: 'projet/satyva', component: SatyvaComponent },
  { path: 'projet/portfolio', component: PortfolioComponent },
  { path: 'projet/administration', component: AdministrationComponent },
  { path: 'projet/bilan-social', component: BilanSocialComponent },
  { path: 'projet/amp', component: AmpComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
